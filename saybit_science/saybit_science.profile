<?php
//$ID$


function saybit_science_profile_details () {
    return array (
      'name' => 'Saybit-Science!',
      'description' => 'Select this profile if you will be using this installation to host Saybit-Science.',
    );
}

function saybit_science_profile_modules () {
    return array (
      //core necessities
      'devel',
      'taxonomy',
      'comment', 
      'contact', 
      'locale',
      'menu',
      'php',
      'search', 
      'field_indexer',
      'cck_field_indexer',
      'upload',
      'token', 
      'views', 
      'views_ui',
      'smtp', 
      'backup_migrate',
      'poormanscron', 
      'menu_breadcrumb',
      //cck necessities
      'content', 
      'content_copy', 
      'fieldgroup', 
      'filefield', 
      'imagefield',  
      'userreference', 
      'number', 
      'optionwidgets', 
      'text', 
      'date_api', 
      'date_timezone', 
      'date', 
      'date_tools', 
      'date_popup',
      'date_locale',
      'hierarchical_select',
      'hs_taxonomy', 
      //image processing necessities 
      'imageapi', 
      'imageapi_gd',
      'imagecache', 
      'imagecache_ui',
      'thickbox',
      //user necessities
	  //mail, messaging, notifications 	
	);
}

/**
 * return an array of modules needed for saybit features supplied out of the box
 * @return unknown_type
 */
function _saybit_science_features_modules() {
    return array (
    //features support
    'ctools',
    'strongarm',
    'context',
    'context_contrib',
    'context_ui',
    'features',
    //************saybit_ui
    'nice_menus',
    'admin',
    'comment_upload',
    'content_profile',
    'diff',
    'flag',
    'hs_taxonomy',
    'path',
    'saybit_relativity',
    'statistics',
    'nodeaccess_userreference',
    //************saybit editor modules
    //'imce',
    //'wysiwyg',
    'saybit_ui',
    //'saybit_science_library',
    //'saybit_editor',
    //'saybit_collaboration',
    );
}

/**
 * config routine for saybit, perform some tasks that override default system values
 * @return unknown_type
 */
function _saybit_science_configure() {
	
  // Remove default input filter formats
  $result = db_query("SELECT * FROM {filter_formats} WHERE name IN ('%s', '%s')", 'Filtered HTML', 'Full HTML');
  while ($row = db_fetch_object($result)) {
    db_query("DELETE FROM {filter_formats} WHERE format = %d", $row->format);
    db_query("DELETE FROM {filters} WHERE format = %d", $row->format);
  }
  // Add Editor filter format for wysiwyg
  $filter_format = array (
    'name' => 'Editor',
    'roles' => ',2,',
    'cache' => 1,
  );
  drupal_write_record('filter_formats', $filter_format);
  $filter_id = db_result(db_query("SELECT format FROM {filter_formats} WHERE (name='Editor')"));
  $filters = array(
    1 => array (
      'format' => $filter_id,
      'module' => 'filter',
      'delta' => 3,
      'weight' => 10,
    ),
    2 => array (
      'format' => $filter_id,
      'module' => 'filter',
      'delta' => 2,
      'weight' => 10,
    ), 
    3 => array (
      'format' => $filter_id,
      'module' => 'filter',
      'delta' => 1,
      'weight' => 10,
    ),
  );
  foreach ($filters as $filter){
  	drupal_write_record('filters', $filter);
  }

  
  // remove perm: 'access content' for anonymous users.
  db_query("UPDATE {permission} set perm = '' WHERE rid = 1");
  
  // Create user picture directory
  $picture_path = file_create_path(variable_get('user_picture_path', 'pictures'));
  file_check_directory($picture_path, 1, 'user_picture_path');
  
  // Set a default time zone
  $tz_offset = date('Z');
  variable_set('date_default_timezone', $tz_offset);

  // Set a default footer message.
  variable_set('site_footer', '&copy; 2010 '. l('Saybit, LLC', 'http://www.saybit.com', array('absolute' => TRUE)));
  
}

/**
 * we need a second stage of configuration to let system finish some processes prior to
 * doing the tasks needed here such as setting the theme.
 * @return unknown_type
 */
function _saybit_science_configure_A() {
  // This may not be needed, but we run it to prevent the "rebuild node access" message from being
  // shown on install.
  node_access_rebuild();

  // Rebuild key tables/caches
  drupal_flush_all_caches();

  // Set SaybitTheme as the default theme. 
  // First we have to run a special system_theme_data routine to detect themes in 
  // the profile directory.
  _saybit_science_system_theme_data();
  db_query("UPDATE {blocks} SET status = 0, region = ''"); 
  db_query("UPDATE {system} SET status = 0 WHERE type = 'theme' and name ='%s'", 'garland');
  db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name ='%s'", 'SaybitTheme');
  variable_set('theme_default', 'SaybitTheme');

  // Delete the old profile content type
  node_type_delete('profile');
}

function _saybit_science_configure_finished($success, $results) {
  //set library feature to installed to run config routine inside library.module
  //we do this becasue hook_enable is not being fired inside library.module for some reason
  _saybit_ui_config();
  //_saybit_science_library_config();
  //saybit_editor_enable();
  //saybit_collaboration_enable();
  variable_set('install_task', 'profile-finished');
  return; 
}


/**
 * following modules load advance installer to the saybit configuration step
 * @return unknown_type
 */
function _saybit_science_profile_batch_finished($success, $results) {
	variable_set('install_task', 'configure-saybit-science');
	return;
}

/**
 * Reimplementation of system_theme_data(). The core function's static cache
 * is populated during install prior to active install profile awareness.
 * This workaround makes enabling themes in profiles/[profile]/themes possible.
 */
function _saybit_science_system_theme_data() {
  global $profile;
  $profile = 'saybit_science';

  $themes = drupal_system_listing('\.info$', 'themes');
  $engines = drupal_system_listing('\.engine$', 'themes/engines');

  $defaults = system_theme_default();

  $sub_themes = array();
  foreach ($themes as $key => $theme) {
    $themes[$key]->info = drupal_parse_info_file($theme->filename) + $defaults;

    if (!empty($themes[$key]->info['base theme'])) {
      $sub_themes[] = $key;
    }

    $engine = $themes[$key]->info['engine'];
    if (isset($engines[$engine])) {
      $themes[$key]->owner = $engines[$engine]->filename;
      $themes[$key]->prefix = $engines[$engine]->name;
      $themes[$key]->template = TRUE;
    }

    // Give the stylesheets proper path information.
    $pathed_stylesheets = array();
    foreach ($themes[$key]->info['stylesheets'] as $media => $stylesheets) {
      foreach ($stylesheets as $stylesheet) {
        $pathed_stylesheets[$media][$stylesheet] = dirname($themes[$key]->filename) .'/'. $stylesheet;
      }
    }
    $themes[$key]->info['stylesheets'] = $pathed_stylesheets;

    // Give the scripts proper path information.
    $scripts = array();
    foreach ($themes[$key]->info['scripts'] as $script) {
      $scripts[$script] = dirname($themes[$key]->filename) .'/'. $script;
    }
    $themes[$key]->info['scripts'] = $scripts;

    // Give the screenshot proper path information.
    if (!empty($themes[$key]->info['screenshot'])) {
      $themes[$key]->info['screenshot'] = dirname($themes[$key]->filename) .'/'. $themes[$key]->info['screenshot'];
    }
  }

  foreach ($sub_themes as $key) {
    $themes[$key]->base_themes = system_find_base_themes($themes, $key);
    // Don't proceed if there was a problem with the root base theme.
    if (!current($themes[$key]->base_themes)) {
      continue;
    }
    $base_key = key($themes[$key]->base_themes);
    foreach (array_keys($themes[$key]->base_themes) as $base_theme) {
      $themes[$base_theme]->sub_themes[$key] = $themes[$key]->info['name'];
    }
    // Copy the 'owner' and 'engine' over if the top level theme uses a
    // theme engine.
    if (isset($themes[$base_key]->owner)) {
      if (isset($themes[$base_key]->info['engine'])) {
        $themes[$key]->info['engine'] = $themes[$base_key]->info['engine'];
        $themes[$key]->owner = $themes[$base_key]->owner;
        $themes[$key]->prefix = $themes[$base_key]->prefix;
      }
      else {
        $themes[$key]->prefix = $key;
      }
    }
  }

  // Extract current files from database.
  system_get_files_database($themes, 'theme');
  db_query("DELETE FROM {system} WHERE type = 'theme'");
  foreach ($themes as $theme) {
    $theme->owner = !isset($theme->owner) ? '' : $theme->owner;
    db_query("INSERT INTO {system} (name, owner, info, type, filename, status, throttle, bootstrap) VALUES ('%s', '%s', '%s', '%s', '%s', %d, %d, %d)", $theme->name, $theme->owner, serialize($theme->info), 'theme', $theme->filename, isset($theme->status) ? $theme->status : 0, 0, 0);
  }
}

/**
 * Implementation of hook_profile_task_list().
 */
function saybit_science_profile_task_list() {
  $tasks['features-modules-batch'] = st('Install features modules');
  $tasks['saybit-science-configure-batch'] = st('Configure Saybit-Science');
  return $tasks;
}

function saybit_science_profile_tasks (&$task, $url) {
	global $install_locale;
	$continue = '<p>'.l(st('Continue'), $url).'</p>';
	$output = '';
	if ($task == 'profile'){
			$task = 'features-modules';
			return $output .= $continue;
	}
  	// We are running a batch task for this profile so basically do nothing and return page
  	if (in_array($task, array('features-modules-batch', 'saybit-science-configure-batch'))) {
    	include_once 'includes/batch.inc';
    	$output = _batch_page();
 	}
 	
	if ($task == 'features-modules') {
		$modules = _saybit_science_features_modules();
		$files = module_rebuild_cache();
	    // Create batch
    	foreach ($modules as $module) {
      		$batch['operations'][] = array('_install_module_batch', array($module, $files[$module]->info['name']));
    	}    
		$batch['finished'] = '_saybit_science_profile_batch_finished';
		$batch['title'] = st('Installing @drupal features', array('@drupal' => drupal_install_profile_name()));
		$batch['error_message'] = st('The installation has encountered an error.');
		variable_set('install_task', 'features-modules-batch');
		batch_set($batch);
		batch_process($url, $url);
	}
	
	if ($task == 'configure-saybit-science') {
		$batch['title'] = st('Configuring @drupal', array('@drupal' => drupal_install_profile_name()));
		$batch['operations'][] = array('_saybit_science_configure', array());
		$batch['operations'][] = array('_saybit_science_configure_A', array());
		$batch['finished'] = '_saybit_science_configure_finished';
		variable_set('install_task', 'saybit-science-configure-batch');
		batch_set($batch);
		batch_process($url, $url);
	}

	return $output;
}