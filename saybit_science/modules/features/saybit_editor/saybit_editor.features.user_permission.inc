<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function saybit_editor_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer imce(execute PHP)
  $permissions['administer imce(execute PHP)'] = array(
    'name' => 'administer imce(execute PHP)',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  return $permissions;
}
