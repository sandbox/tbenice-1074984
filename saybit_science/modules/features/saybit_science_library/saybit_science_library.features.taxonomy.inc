<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function saybit_science_library_taxonomy_default_vocabularies() {
  return array(
    'data_store_categories' => array(
      'name' => 'Data Store-Categories',
      'description' => 'Categorize your Data Stores to find them easier.',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_data_store_categories',
      'weight' => '0',
      'type' => NULL,
      'nodes' => array(),
    ),
    'document_categories' => array(
      'name' => 'Document-Categories',
      'description' => 'Categorize your generic documents to find them easier.',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_document_categories',
      'weight' => '0',
      'type' => NULL,
      'nodes' => array(),
    ),
    'project_categories' => array(
      'name' => 'Project-Categories',
      'description' => 'Categorize your projects to find them easier.',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_project_categories',
      'weight' => '0',
      'type' => NULL,
      'nodes' => array(),
    ),
  );
}
