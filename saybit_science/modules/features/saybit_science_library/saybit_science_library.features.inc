<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function saybit_science_library_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function saybit_science_library_node_info() {
  $items = array(
    'data_store' => array(
      'name' => t('Data Store'),
      'module' => 'features',
      'description' => t('A data_store is the execution of a protocol and results in data generation (e.g. behavior experiment, western blot, DNA gel, etc).'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'discussion' => array(
      'name' => t('Discussion'),
      'module' => 'features',
      'description' => t('Start a discussion with colleagues about a specific topic.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'has_body' => '1',
      'body_label' => t('Opener'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'document' => array(
      'name' => t('Document'),
      'module' => 'features',
      'description' => t('Any kind of page that can be read by all'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'project' => array(
      'name' => t('Project'),
      'module' => 'features',
      'description' => t('A project is owned by a project manager and contains experiments, manuscripts, etc'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function saybit_science_library_views_api() {
  return array(
    'api' => '2',
  );
}
