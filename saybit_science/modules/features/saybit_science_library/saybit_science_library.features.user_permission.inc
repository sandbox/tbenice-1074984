<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function saybit_science_library_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer url aliases
  $permissions['administer url aliases'] = array(
    'name' => 'administer url aliases',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: create data_store content
  $permissions['create data_store content'] = array(
    'name' => 'create data_store content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: create discussion content
  $permissions['create discussion content'] = array(
    'name' => 'create discussion content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: create document content
  $permissions['create document content'] = array(
    'name' => 'create document content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: create project content
  $permissions['create project content'] = array(
    'name' => 'create project content',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: create url aliases
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete any data_store content
  $permissions['delete any data_store content'] = array(
    'name' => 'delete any data_store content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: delete any discussion content
  $permissions['delete any discussion content'] = array(
    'name' => 'delete any discussion content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: delete any document content
  $permissions['delete any document content'] = array(
    'name' => 'delete any document content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: delete any project content
  $permissions['delete any project content'] = array(
    'name' => 'delete any project content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: delete own data_store content
  $permissions['delete own data_store content'] = array(
    'name' => 'delete own data_store content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete own discussion content
  $permissions['delete own discussion content'] = array(
    'name' => 'delete own discussion content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete own document content
  $permissions['delete own document content'] = array(
    'name' => 'delete own document content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete own project content
  $permissions['delete own project content'] = array(
    'name' => 'delete own project content',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: edit any data_store content
  $permissions['edit any data_store content'] = array(
    'name' => 'edit any data_store content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: edit any discussion content
  $permissions['edit any discussion content'] = array(
    'name' => 'edit any discussion content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: edit any document content
  $permissions['edit any document content'] = array(
    'name' => 'edit any document content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: edit any project content
  $permissions['edit any project content'] = array(
    'name' => 'edit any project content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: edit own data_store content
  $permissions['edit own data_store content'] = array(
    'name' => 'edit own data_store content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: edit own discussion content
  $permissions['edit own discussion content'] = array(
    'name' => 'edit own discussion content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: edit own document content
  $permissions['edit own document content'] = array(
    'name' => 'edit own document content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: edit own project content
  $permissions['edit own project content'] = array(
    'name' => 'edit own project content',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  return $permissions;
}
