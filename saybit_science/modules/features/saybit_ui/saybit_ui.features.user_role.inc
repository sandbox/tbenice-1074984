<?php

/**
 * Implementation of hook_user_default_roles().
 */
function saybit_ui_user_default_roles() {
  $roles = array();

  // Exported role: Saybit Administrator
  $roles['Saybit Administrator'] = array(
    'name' => 'Saybit Administrator',
  );

  // Exported role: Site Administrator
  $roles['Site Administrator'] = array(
    'name' => 'Site Administrator',
  );

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  // Exported role: authenticated user
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
  );
  
    // Exported role: Experimenter
  $roles['Experimenter'] = array(
    'name' => 'Experimenter',
  );

  // Exported role: Project Manager
  $roles['Project Manager'] = array(
    'name' => 'Project Manager',
  );

  return $roles;
}
