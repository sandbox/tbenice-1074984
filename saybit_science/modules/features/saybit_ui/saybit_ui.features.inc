<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function saybit_ui_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_homebox().
 */
function saybit_ui_homebox() {
  $homeboxes = array();

  $homeboxes['dashboard'] = array (
    'regions' => 3,
    'cache' => 0,
    'color' => 0,
    'colors' => 
    array (
      0 => '#E4F0F8',
      1 => '#E4F0F8',
      2 => '#E4F0F8',
      3 => '#E4F0F8',
      4 => '#E4F0F8',
      5 => '#E4F0F8',
    ),
    'blocks' => 
    array (
      'views_4204b29569a0057261d105a10d878cbb' => 
      array (
        'module' => 'views',
        'delta' => '4204b29569a0057261d105a10d878cbb',
        'region' => 3,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -23,
      ),
      'views_user_data_stores-block_1' => 
      array (
        'module' => 'views',
        'delta' => 'user_data_stores-block_1',
        'region' => 2,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -23,
      ),
      'views_602a47cb0a2deac782ef125c1ec867aa' => 
      array (
        'module' => 'views',
        'delta' => '602a47cb0a2deac782ef125c1ec867aa',
        'region' => 3,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -22,
      ),
      'views_user_projects-block_1' => 
      array (
        'module' => 'views',
        'delta' => 'user_projects-block_1',
        'region' => 2,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -22,
      ),
      'views_6e372f885271000a895366c37036efde' => 
      array (
        'module' => 'views',
        'delta' => '6e372f885271000a895366c37036efde',
        'region' => 3,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -21,
      ),
      'views_3f7ffdef89ef08af1133e76472ffa2c6' => 
      array (
        'module' => 'views',
        'delta' => '3f7ffdef89ef08af1133e76472ffa2c6',
        'region' => 2,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -21,
      ),
      'saybit_ui_saybit_document_create' => 
      array (
        'module' => 'saybit_ui',
        'delta' => 'saybit_document_create',
        'region' => 1,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 1,
        'title' => 'Create Documents',
        'weight' => -20,
      ),
      'views_afcf83bc0f1120560e642a24918c0146' => 
      array (
        'module' => 'views',
        'delta' => 'afcf83bc0f1120560e642a24918c0146',
        'region' => 2,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -20,
      ),
      'views_saybit_flag_bookmarks-block_1' => 
      array (
        'module' => 'views',
        'delta' => 'saybit_flag_bookmarks-block_1',
        'region' => 1,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -20,
      ),
      'views_58bb4d05db9a5118d164fa0cb1f9dde4' => 
      array (
        'module' => 'views',
        'delta' => '58bb4d05db9a5118d164fa0cb1f9dde4',
        'region' => 2,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -19,
      ),
      'views_49b8960c53b9e3b178ea020a7f820796' => 
      array (
        'module' => 'views',
        'delta' => '49b8960c53b9e3b178ea020a7f820796',
        'region' => 3,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -19,
      ),
      'views_3eb29ce9363b0dde6e67531307aef33e' => 
      array (
        'module' => 'views',
        'delta' => '3eb29ce9363b0dde6e67531307aef33e',
        'region' => 2,
        'movable' => 1,
        'status' => 0,
        'open' => 1,
        'closable' => 1,
        'title' => '',
        'weight' => -18,
      ),
    ),
    'widths' => 
    array (
      1 => 50,
      2 => 50,
      3 => 100,
    ),
    'title' => 'Saybit Dashboard',
    'path' => 'dashboard',
    'menu' => 0,
    'enabled' => 1,
    'full' => 1,
    'custom' => 1,
    'roles' => 
    array (
      0 => 'Experimenter',
      1 => 'Project Manager',
      2 => 'Saybit Administrator',
      3 => 'Site Administrator',
    ),
  );


  return $homeboxes;
}

/**
 * Implementation of hook_node_info().
 */
function saybit_ui_node_info() {
  $items = array(
    'cv_profile' => array(
      'name' => t('Curriculum Vitae'),
      'module' => 'features',
      'description' => t('A user\'s Curriculum Vitae for use in writing grants among other uses.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function saybit_ui_views_api() {
  return array(
    'api' => '2',
  );
}
