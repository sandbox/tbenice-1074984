<?php

/**
 * Implementation of hook_content_default_fields().
 */
function saybit_ui_content_default_fields() {
  $fields = array();

  // Exported field: field_cv_address
  $fields['cv_profile-field_cv_address'] = array(
    'field_name' => 'field_cv_address',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => 'group_cv_contact',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_address][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Address',
      'weight' => '-3',
      'description' => '',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_grants
  $fields['cv_profile-field_cv_grants'] = array(
    'field_name' => 'field_cv_grants',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_grants][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Grants Received',
      'weight' => '-1',
      'description' => 'Enter a list of grants you have received, starting with active grants and moving down to completed ones.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_honors
  $fields['cv_profile-field_cv_honors'] = array(
    'field_name' => 'field_cv_honors',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '2',
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_honors][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Honors',
      'weight' => '2',
      'description' => 'List your professional honors',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_phone
  $fields['cv_profile-field_cv_phone'] = array(
    'field_name' => 'field_cv_phone',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => 'group_cv_contact',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_phone][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Phone Number',
      'weight' => '-2',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_positions
  $fields['cv_profile-field_cv_positions'] = array(
    'field_name' => 'field_cv_positions',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_positions][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Positions Held',
      'weight' => '-2',
      'description' => 'Enter the professional positions you have held, starting with the most recent.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_publications
  $fields['cv_profile-field_cv_publications'] = array(
    'field_name' => 'field_cv_publications',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => 0,
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_publications][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Selected Publications',
      'weight' => 0,
      'description' => 'Enter a list of selected publications.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_societies
  $fields['cv_profile-field_cv_societies'] = array(
    'field_name' => 'field_cv_societies',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '1',
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_societies][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Professional Societies',
      'weight' => '1',
      'description' => 'Enter a list of professional societies to which you are a member.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_cv_teach
  $fields['cv_profile-field_cv_teach'] = array(
    'field_name' => 'field_cv_teach',
    'type_name' => 'cv_profile',
    'display_settings' => array(
      'weight' => '3',
      'parent' => 'group_cv_professional',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '1',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_cv_teach][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Teaching and Outreach',
      'weight' => '3',
      'description' => 'List your teaching experiences and scientific outreach activities.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Address');
  t('Grants Received');
  t('Honors');
  t('Phone Number');
  t('Positions Held');
  t('Professional Societies');
  t('Selected Publications');
  t('Teaching and Outreach');

  return $fields;
}
