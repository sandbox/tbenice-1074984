<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function saybit_ui_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_cv_contact
  $groups['cv_profile-group_cv_contact'] = array(
    'group_type' => 'standard',
    'type_name' => 'cv_profile',
    'group_name' => 'group_cv_contact',
    'label' => 'Contact Information',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight' => '-4',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-4',
    'fields' => array(
      '0' => 'field_cv_address',
      '1' => 'field_cv_phone',
    ),
  );

  // Exported group: group_cv_professional
  $groups['cv_profile-group_cv_professional'] = array(
    'group_type' => 'standard',
    'type_name' => 'cv_profile',
    'group_name' => 'group_cv_professional',
    'label' => 'Professional Information',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset_collapsible',
        'description' => '',
      ),
      'display' => array(
        'weight' => '-3',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-3',
    'fields' => array(
      '0' => 'field_cv_positions',
      '1' => 'field_cv_grants',
      '2' => 'field_cv_publications',
      '3' => 'field_cv_societies',
      '4' => 'field_cv_honors',
      '5' => 'field_cv_teach',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact Information');
  t('Professional Information');

  return $groups;
}
