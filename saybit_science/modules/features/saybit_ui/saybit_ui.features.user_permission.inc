<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function saybit_ui_user_default_permissions() {
  $permissions = array();

  // Exported permission: Use PHP input for field settings (dangerous - grant with care)
  $permissions['Use PHP input for field settings (dangerous - grant with care)'] = array(
    'name' => 'Use PHP input for field settings (dangerous - grant with care)',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: access administration pages
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: access all views
  $permissions['access all views'] = array(
    'name' => 'access all views',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: access backup and migrate
  $permissions['access backup and migrate'] = array(
    'name' => 'access backup and migrate',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: access backup files
  $permissions['access backup files'] = array(
    'name' => 'access backup files',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: access comments
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: access devel information
  $permissions['access devel information'] = array(
    'name' => 'access devel information',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: access site reports
  $permissions['access site reports'] = array(
    'name' => 'access site reports',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: access site-wide contact form
  $permissions['access site-wide contact form'] = array(
    'name' => 'access site-wide contact form',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: access statistics
  $permissions['access statistics'] = array(
    'name' => 'access statistics',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: access user profiles
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: add content
  $permissions['add content'] = array(
    'name' => 'add content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: administer actions
  $permissions['administer actions'] = array(
    'name' => 'administer actions',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer backup and migrate
  $permissions['administer backup and migrate'] = array(
    'name' => 'administer backup and migrate',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer blocks
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer comments
  $permissions['administer comments'] = array(
    'name' => 'administer comments',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer content types
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer date tools
  $permissions['administer date tools'] = array(
    'name' => 'administer date tools',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer features
  $permissions['administer features'] = array(
    'name' => 'administer features',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer files
  $permissions['administer files'] = array(
    'name' => 'administer files',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer filters
  $permissions['administer filters'] = array(
    'name' => 'administer filters',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer flags
  $permissions['administer flags'] = array(
    'name' => 'administer flags',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer homebox
  $permissions['administer homebox'] = array(
    'name' => 'administer homebox',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer imageapi
  $permissions['administer imageapi'] = array(
    'name' => 'administer imageapi',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer imagecache
  $permissions['administer imagecache'] = array(
    'name' => 'administer imagecache',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer languages
  $permissions['administer languages'] = array(
    'name' => 'administer languages',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer menu
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer nodeaccess
  $permissions['administer nodeaccess'] = array(
    'name' => 'administer nodeaccess',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer nodes
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer permissions
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer search
  $permissions['administer search'] = array(
    'name' => 'administer search',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer site configuration
  $permissions['administer site configuration'] = array(
    'name' => 'administer site configuration',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer site-wide contact form
  $permissions['administer site-wide contact form'] = array(
    'name' => 'administer site-wide contact form',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer smtp module
  $permissions['administer smtp module'] = array(
    'name' => 'administer smtp module',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer taxonomy
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer users
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: administer views
  $permissions['administer views'] = array(
    'name' => 'administer views',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: change own username
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: create cv_profile content
  $permissions['create cv_profile content'] = array(
    'name' => 'create cv_profile content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete any cv_profile content
  $permissions['delete any cv_profile content'] = array(
    'name' => 'delete any cv_profile content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: delete backup files
  $permissions['delete backup files'] = array(
    'name' => 'delete backup files',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: delete own cv_profile content
  $permissions['delete own cv_profile content'] = array(
    'name' => 'delete own cv_profile content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: delete revisions
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: display source code
  $permissions['display source code'] = array(
    'name' => 'display source code',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: edit any cv_profile content
  $permissions['edit any cv_profile content'] = array(
    'name' => 'edit any cv_profile content',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: edit own cv_profile content
  $permissions['edit own cv_profile content'] = array(
    'name' => 'edit own cv_profile content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: execute php code
  $permissions['execute php code'] = array(
    'name' => 'execute php code',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: flush imagecache
  $permissions['flush imagecache'] = array(
    'name' => 'flush imagecache',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: grant deletable node permissions
  $permissions['grant deletable node permissions'] = array(
    'name' => 'grant deletable node permissions',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: grant editable node permissions
  $permissions['grant editable node permissions'] = array(
    'name' => 'grant editable node permissions',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: grant node permissions
  $permissions['grant node permissions'] = array(
    'name' => 'grant node permissions',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: grant own node permissions
  $permissions['grant own node permissions'] = array(
    'name' => 'grant own node permissions',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: manage features
  $permissions['manage features'] = array(
    'name' => 'manage features',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: perform backup
  $permissions['perform backup'] = array(
    'name' => 'perform backup',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: post comments
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: post comments without approval
  $permissions['post comments without approval'] = array(
    'name' => 'post comments without approval',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: restore from backup
  $permissions['restore from backup'] = array(
    'name' => 'restore from backup',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: revert revisions
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: search content
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: select different theme
  $permissions['select different theme'] = array(
    'name' => 'select different theme',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: switch users
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: translate interface
  $permissions['translate interface'] = array(
    'name' => 'translate interface',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: upload files
  $permissions['upload files'] = array(
    'name' => 'upload files',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: upload files to comments
  $permissions['upload files to comments'] = array(
    'name' => 'upload files to comments',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: use PHP for block visibility
  $permissions['use PHP for block visibility'] = array(
    'name' => 'use PHP for block visibility',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: use admin toolbar
  $permissions['use admin toolbar'] = array(
    'name' => 'use admin toolbar',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: use advanced search
  $permissions['use advanced search'] = array(
    'name' => 'use advanced search',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: view date repeats
  $permissions['view date repeats'] = array(
    'name' => 'view date repeats',
    'roles' => array(
      '0' => 'Project Manager',
      '1' => 'Saybit Administrator',
      '2' => 'Site Administrator',
    ),
  );

  // Exported permission: view files uploaded to comments
  $permissions['view files uploaded to comments'] = array(
    'name' => 'view files uploaded to comments',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: view imagecache saybit
  $permissions['view imagecache saybit'] = array(
    'name' => 'view imagecache saybit',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: view imagecache saybit-small
  $permissions['view imagecache saybit-small'] = array(
    'name' => 'view imagecache saybit-small',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: view post access counter
  $permissions['view post access counter'] = array(
    'name' => 'view post access counter',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: view revisions
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: view uploaded files
  $permissions['view uploaded files'] = array(
    'name' => 'view uploaded files',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  return $permissions;
}
