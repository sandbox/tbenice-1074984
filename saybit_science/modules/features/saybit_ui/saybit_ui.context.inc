<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function saybit_ui_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'saybit_ui-section-site';
  $context->description = 'main ui context for all pages';
  $context->tag = 'Saybit';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'saybit_ui-Welcome-Message' => array(
          'module' => 'saybit_ui',
          'delta' => 'Welcome-Message',
          'region' => 'header_branding',
          'weight' => 0,
        ),
        'nice_menus-1' => array(
          'module' => 'nice_menus',
          'delta' => 1,
          'region' => 'header_menu',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Saybit');
  t('main ui context for all pages');

  $export['saybit_ui-section-site'] = $context;
  return $export;
}
