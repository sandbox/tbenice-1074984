<?php
// $Id: saybit_relativity.views.inc,v 1.1.2.3 2010/08/02 12:29:20 davea Exp $

function saybit_relativity_views_data() {
  //drupal_set_message('View hook called.');
  $data['saybit_relativity']['table']['group'] = t('Node saybit_relativity');

  $data['saybit_relativity']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Node ID'),
    'help' => t('The Node ID of the parent node.'),
  );

  $data['saybit_relativity']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['saybit_relativity']['nid'] = array(
    'title' => t('Child Node ID'),
    'help' => t('The Node ID of the child node.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
  );

  $data['saybit_relativity']['parent_nid'] = array(
    'title' => t('Parent Node ID'),
    'help' => t('The Node ID of the parent node.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );

  return $data;
}
