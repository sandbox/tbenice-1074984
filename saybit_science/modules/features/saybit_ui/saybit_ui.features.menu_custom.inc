<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function saybit_ui_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-functions
  $menus['main-functions'] = array(
    'menu_name' => 'main-functions',
    'title' => 'Main Functions',
    'description' => 'The Primary Saybit Menubar',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main Functions');
  t('The Primary Saybit Menubar');


  return $menus;
}
