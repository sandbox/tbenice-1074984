<?php

/**
 * Implementation of hook_strongarm().
 */
function saybit_collaboration_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'messaging_default_method';
  $strongarm->value = 'mail';

  $export['messaging_default_method'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'messaging_method_enabled';
  $strongarm->value = array(
    'mail' => 'mail',
  );

  $export['messaging_method_enabled'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'messaging_method_mail';
  $strongarm->value = array(
    'name' => 'Mail',
    'queue' => 0,
    'log' => 0,
  );

  $export['messaging_method_mail'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_content_per_type';
  $strongarm->value = '0';

  $export['notifications_content_per_type'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_content_type';
  $strongarm->value = array(
    'thread' => 'thread',
    'nodetype' => 'nodetype',
    'author' => 'author',
    'typeauthor' => 'typeauthor',
  );

  $export['notifications_content_type'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_default_auto';
  $strongarm->value = 1;

  $export['notifications_default_auto'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_default_send_interval';
  $strongarm->value = '0';

  $export['notifications_default_send_interval'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_event_enabled';
  $strongarm->value = array(
    'node-insert' => 1,
    'node-document-insert' => 1,
    'node-data_store-insert' => 1,
    'node-project-insert' => 1,
    'node-cv_profile-insert' => 1,
    'node-update' => 1,
    'node-document-update' => 1,
    'node-data_store-update' => 1,
    'node-project-update' => 1,
    'node-cv_profile-update' => 1,
    'node-comment' => 1,
    'node-document-comment' => 1,
    'node-data_store-comment' => 1,
    'node-project-comment' => 1,
    'node-cv_profile-comment' => 1,
  );

  $export['notifications_event_enabled'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_event_template';
  $strongarm->value = array(
    'node-insert' => 'notifications-event-node-insert',
    'node-document-insert' => 'notifications-event-node-insert',
    'node-data_store-insert' => 'notifications-event-node-insert',
    'node-project-insert' => 'notifications-event-node-insert',
    'node-cv_profile-insert' => 'notifications-event-node-insert',
    'node-update' => 'notifications-event-node-update',
    'node-document-update' => 'notifications-event-node-update',
    'node-data_store-update' => 'notifications-event-node-update',
    'node-project-update' => 'notifications-event-node-update',
    'node-cv_profile-update' => 'notifications-event-node-update',
    'node-comment' => 'notifications-event-node-comment',
    'node-document-comment' => 'notifications-event-node-comment',
    'node-data_store-comment' => 'notifications-event-node-comment',
    'node-project-comment' => 'notifications-event-node-comment',
    'node-cv_profile-comment' => 'notifications-event-node-comment',
  );

  $export['notifications_event_template'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_send_immediate';
  $strongarm->value = 1;

  $export['notifications_send_immediate'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_sender';
  $strongarm->value = '0';

  $export['notifications_sender'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_sendself';
  $strongarm->value = 0;

  $export['notifications_sendself'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_subscription_types';
  $strongarm->value = array(
    'thread' => 'thread',
    'nodetype' => 'nodetype',
    'author' => 'author',
    'typeauthor' => 'typeauthor',
  );

  $export['notifications_subscription_types'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_team_max_options';
  $strongarm->value = '20';

  $export['notifications_team_max_options'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_team_options';
  $strongarm->value = array(
    'node' => 'node',
    'comment' => 'comment',
  );

  $export['notifications_team_options'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_team_per_type';
  $strongarm->value = '0';

  $export['notifications_team_per_type'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_team_user_view';
  $strongarm->value = '';

  $export['notifications_team_user_view'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_account_options';
  $strongarm->value = array(
    'links' => 'links',
    'block' => 'block',
  );

  $export['notifications_ui_account_options'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_node_options';
  $strongarm->value = array(
    'block' => 'block',
    'form' => 0,
    'comment' => 0,
    'links' => 0,
    'teaserlinks' => 0,
    'subform' => 0,
  );

  $export['notifications_ui_node_options'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_per_type';
  $strongarm->value = '0';

  $export['notifications_ui_per_type'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_subscribe_links';
  $strongarm->value = '1';

  $export['notifications_ui_subscribe_links'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_types';
  $strongarm->value = array(
    'thread' => 'thread',
    'nodetype' => 'nodetype',
    'author' => 'author',
    'typeauthor' => 'typeauthor',
  );

  $export['notifications_ui_types'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_unsubscribe_links';
  $strongarm->value = '1';

  $export['notifications_ui_unsubscribe_links'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_ui_user_options';
  $strongarm->value = array(
    'page' => 'page',
    'create' => 'create',
  );

  $export['notifications_ui_user_options'] = $strongarm;
  return $export;
}
