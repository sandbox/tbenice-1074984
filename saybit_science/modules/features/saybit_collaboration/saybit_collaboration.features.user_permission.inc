<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function saybit_collaboration_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer messaging
  $permissions['administer messaging'] = array(
    'name' => 'administer messaging',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: administer notifications
  $permissions['administer notifications'] = array(
    'name' => 'administer notifications',
    'roles' => array(
      '0' => 'Saybit Administrator',
    ),
  );

  // Exported permission: edit message templates
  $permissions['edit message templates'] = array(
    'name' => 'edit message templates',
    'roles' => array(
      '0' => 'Saybit Administrator',
      '1' => 'Site Administrator',
    ),
  );

  // Exported permission: maintain own subscriptions
  $permissions['maintain own subscriptions'] = array(
    'name' => 'maintain own subscriptions',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: manage all subscriptions
  $permissions['manage all subscriptions'] = array(
    'name' => 'manage all subscriptions',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: manage own subscriptions
  $permissions['manage own subscriptions'] = array(
    'name' => 'manage own subscriptions',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: skip notifications
  $permissions['skip notifications'] = array(
    'name' => 'skip notifications',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: subscribe other users
  $permissions['subscribe other users'] = array(
    'name' => 'subscribe other users',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: subscribe to author
  $permissions['subscribe to author'] = array(
    'name' => 'subscribe to author',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: subscribe to content
  $permissions['subscribe to content'] = array(
    'name' => 'subscribe to content',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: subscribe to content type
  $permissions['subscribe to content type'] = array(
    'name' => 'subscribe to content type',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  // Exported permission: subscribe to content type and author
  $permissions['subscribe to content type and author'] = array(
    'name' => 'subscribe to content type and author',
    'roles' => array(
      '0' => 'Experimenter',
      '1' => 'Project Manager',
      '2' => 'Saybit Administrator',
      '3' => 'Site Administrator',
    ),
  );

  return $permissions;
}
