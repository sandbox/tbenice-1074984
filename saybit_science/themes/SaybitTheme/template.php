<?php
/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $type
 *
 * @see search-result.tpl.php
 */
function SaybitTheme_preprocess_search_result(&$variables) {
  global $user;
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = check_plain($result['title']);
  if (node_access('view', node_load($result['node']->nid), $user)){
    $variables['view_link'] = $variables['url'];
  }
  if (node_access('update', node_load($result['node']->nid), $user)){
    $variables['edit_link'] = 'node/' .$result['node']->nid . '/edit';
  }  
  $info = array();
  if (!empty($result['type'])) {
    $info['type'] = check_plain($result['type']);
  }
  if (!empty($result['user'])) {
    $info['user'] = $result['user'];
  }
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'small');
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    $info = array_merge($info, $result['extra']);
  }
  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';
  // Provide separated and grouped meta information..
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
  // Provide alternate search result template.
  $variables['template_files'][] = 'search-result-'. $variables['type'];
}

/**
 * Override helper function that builds the nested lists of a nice menu.
 * This override provides nowherelinks capability
 *
 *
 * @param $menu
 *   Menu array from which to build the nested lists.
 */
function phptemplate_nice_menu_build($menu) {
  $output = '';

  foreach ($menu as $menu_item) {
    $mlid = $menu_item['link']['mlid'];
    // Check to see if it is a visible menu item.
    if ($menu_item['link']['hidden'] == 0) {
      // Build class name based on menu path
      // e.g. to give each menu item individual style.
      // Strip funny symbols.
      $clean_path = str_replace(array('http://', '<', '>', '&', '=', '?', ':'), '', $menu_item['link']['href']);
      // Convert slashes to dashes.
      $clean_path = str_replace('/', '-', $clean_path);
      $path_class = 'menu-path-'. $clean_path;
      $nowhere_links = module_invoke_all('nowherelinks');
      // If it has children build a nice little tree under it.
      if ((!empty($menu_item['link']['has_children'])) && (!empty($menu_item['below']))) {
        // Keep passing children into the function 'til we get them all.
        $children = theme('nice_menu_build', $menu_item['below']);
        // Set the class to parent only of children are displayed.
        $parent_class = $children ? 'menuparent ' : '';
        if (!(in_array($menu_item['link']['link_path'], $nowhere_links))) {
        //if (!($menu_item['link']['link_path'] == "node")) {
            $output .= '<li id="menu-'. $mlid .'" class="'. $parent_class . $path_class .'">'. theme('menu_item_link', $menu_item['link']);
        }
        else {
            $output .= '<li id="menu-'. $mlid .'" class="'. $parent_class . $path_class .'">' . '<a href="#">' . $menu_item['link']['title'] . '</a>';
            //print_r($menu_item);     
        }
        // Build the child UL only if children are displayed for the user.
        if ($children) {
          $output .= '<ul>';
          $output .= $children;
          $output .= "</ul>\n";
        }
        $output .= "</li>\n";
      }
      else {
        if (!(in_array($menu_item['link']['link_path'], $nowhere_links))) {
        //if (!($menu_item['link']['link_path'] == "node")) {
            $output .= '<li id="menu-'. $mlid .'" class="'. $path_class .'">'. theme('menu_item_link', $menu_item['link']) .'</li>'."\n";
        }
        else {
            $output .= '<li id="menu-'. $mlid .'" class="'. $path_class .'">'. '<a href="#">' . $menu_item['link']['title'] . '</a>' .'</li>'."\n";
        }
      }
    }
  }
  return $output;
} 